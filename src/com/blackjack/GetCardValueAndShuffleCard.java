package com.blackjack;

import java.util.Vector;

public class GetCardValueAndShuffleCard {

	public int getCardValue(int card)
	{
		int result = card;
		switch (card)
		{
		case 11:
		case 12:
		case 13:
			result =  10;
		}
		return result;
	}
	public String showCard(int card)
	{
		switch (card)
		{
		case 1:
			return "Ace";
		case 2:
			return "2";
		case 3:
			return "3";
		case 4:
			return "4";
		case 5:
			return "5";
		case 6:
			return "6";
		case 7:
			return "7";
		case 8:
			return "8";
		case 9:
			return "9";
		case 10:
			return "10";
		case 11:
			return "Jack";
		case 12:
			return "Queen";
		case 13:
			return "King";
		default:
			return "??";
		}
	}

	public int value(Vector cardsInHand)
	{

		int val;      
		boolean ace;  
		int cards;  

		val = 0;
		ace = false;
		cards = cardsInHand.size();

		for (int i = 0; i < cards; i++)
		{
			int card;    
			int cardVal;  
			card = getCard(cardsInHand, i);

			if(card > 10){
				card = 10;
			}
			cardVal = card;  
			if (cardVal > 10)
			{
				cardVal = 10;   
			}
			if (cardVal == 1)
			{
				ace = true;     
			}
			val = val + cardVal;
		}

		if (ace == true && val + 10 <= 21)
		{
			val = val + 10;
		}

		return val;

	}

	public int getCard(Vector cardsInHand, int position)
	{

		if (position >= 0 && position < cardsInHand.size())
		{
			return ((Integer)cardsInHand.elementAt(position)).intValue();
		} else
		{
			return 0;
		}
	}


	public int dealCard(int[] deck, int currentPosition)
	{
		if (currentPosition == 52)
		{
			shuffleCards(deck, currentPosition);
		}
		currentPosition++;
		return deck[currentPosition - 1];
	}

	public void shuffleCards(int[] deck, int currentPosition)
	{
		for (int i = 51; i > 0; i--)
		{
			int rand = (int) (Math.random() * (i + 1));
			int temp = deck[i];
			deck[i] = deck[rand];
			deck[rand] = temp;
		}
		currentPosition = 0;
	}

}
