package com.blackjack;

import java.util.Scanner;
import java.util.Vector;

public class PlayBlackjack {
	private static Scanner scanner = new Scanner(System.in);
	private int[] deck;   //  52 Cards
	private int positionInDeck; 
	private Vector cardsInHand;
	private int currentPosition; // Current position in the deck
	private Vector hand;   // The cards in the hand.
	GetCardValueAndShuffleCard getCardValueAndShuffleCard = null;

	public boolean playBlackjack()
	{

		Vector dealerCardInHand;   
		Vector userCardInHand, userCardInHand2, userCardInHand3 ;
		getCardValueAndShuffleCard = new GetCardValueAndShuffleCard();

		// Create cards.
		deck = new int[52];
		int cardCreated = 0; 
		for (int c = 0; c <= 3; c++)
		{
			for (int value = 1; value <= 13; value++)
			{
				deck[cardCreated] = value;
				cardCreated++;
			}
		}
		positionInDeck = 0;

		dealerCardInHand = new Vector();
		userCardInHand =   new Vector();
		userCardInHand2 =  new Vector();
		userCardInHand3 =  new Vector();



		shuffleCards();
		dealerCardInHand.addElement(dealCard());
		dealerCardInHand.addElement(dealCard());
		userCardInHand.addElement(dealCard());
		userCardInHand.addElement(dealCard());

		userCardInHand2.addElement(dealCard());
		userCardInHand2.addElement(dealCard());
		userCardInHand3.addElement(dealCard());
		userCardInHand3.addElement(dealCard());


		System.out.println();
		System.out.println();

		//Check if one of the players has Blackjack (two cards totaling to 21).

		if (value(dealerCardInHand) == 21)
		{
			System.out.println("Dealer has the " + getCardValueAndShuffleCard.showCard(getCardValueAndShuffleCard.getCard(dealerCardInHand, 0)) + " and the " + 
					getCardValueAndShuffleCard.showCard(getCardValueAndShuffleCard.getCard(dealerCardInHand, 1)) + ".");
			System.out.println("User has the " + getCardValueAndShuffleCard.showCard(getCardValueAndShuffleCard.getCard(userCardInHand, 0)) + " and the " + 
					getCardValueAndShuffleCard.showCard(getCardValueAndShuffleCard.getCard(userCardInHand, 1)) + ".");
			System.out.println();
			System.out.println("Dealer has Blackjack.  Dealer wins.");
			return false;
		}

		if (value(userCardInHand) == 21 || value(userCardInHand) == 21 || value(userCardInHand) == 21)
		{
			System.out.println("Dealer has the " + getCardValueAndShuffleCard.showCard(getCardValueAndShuffleCard.getCard(dealerCardInHand, 0)) + " and the " + 
					getCardValueAndShuffleCard.showCard(getCardValueAndShuffleCard.getCard(dealerCardInHand, 1)) + ".");
			System.out.println("User has the " + getCardValueAndShuffleCard.showCard(getCardValueAndShuffleCard.getCard(userCardInHand, 0)) + " and the " + 
					getCardValueAndShuffleCard.showCard(getCardValueAndShuffleCard.getCard(userCardInHand, 1)) + ".");
			System.out.println();
			System.out.println("You have Blackjack.  You win.");
			return true;
		}

		for(;;)
		{

			System.out.println();
			System.out.println("Player's cards are:");
			System.out.println(" Player 1   " );
			for (int i = 0; i < userCardInHand.size(); i++)
			{
				System.out.print("    " + getCardValueAndShuffleCard.showCard(getCardValueAndShuffleCard.getCard(userCardInHand, i)));
			}
			System.out.println("");
			System.out.println(" Player 2   " );
			for (int i = 0; i < userCardInHand2.size(); i++)
			{
				System.out.print("    " + getCardValueAndShuffleCard.showCard(getCardValueAndShuffleCard.getCard(userCardInHand2, i)));
			}
			System.out.println("");
			System.out.println(" Player 3   " );
			for (int i = 0; i < userCardInHand3.size(); i++)
			{
				System.out.print("    " + getCardValueAndShuffleCard.showCard(getCardValueAndShuffleCard.getCard(userCardInHand3, i)));
			}
			System.out.println("");
			System.out.println("Player 1 total is " + value(userCardInHand));
			System.out.println();
			System.out.println("Player 2 total is " + value(userCardInHand2));
			System.out.println();
			System.out.println("Player 3 total is " + value(userCardInHand3));
			System.out.println();

			System.out.println("Dealer is showing the " + getCardValueAndShuffleCard.showCard(getCardValueAndShuffleCard.getCard(dealerCardInHand, 0)));
			System.out.println();
			System.out.print("Hit (H) or Stand (S)? ");
			char userAction;  

			userAction = Character.toUpperCase(scanner.next().charAt(0));
			while (userAction != 'H' && userAction != 'S'){
				if (userAction != 'H' && userAction != 'S')
				{
					System.out.print("Please respond H or S:  ");
					break;
				}
			} 

			if (userAction == 'S')
			{
				break;
			} else
			{  
				// Give the user a card.  If the user goes over 21, the user loses.
				int newCard = dealCard();
				int newCard2 = dealCard();
				int newCard3 = dealCard();

				userCardInHand.addElement(newCard);
				userCardInHand2.addElement(newCard2);
				userCardInHand3.addElement(newCard3);
				System.out.println();
				System.out.println("User hits.");
				System.out.println("Player 1 card is the " + getCardValueAndShuffleCard.showCard(newCard));
				System.out.println("Player 2 card is the " + getCardValueAndShuffleCard.showCard(newCard2));
				System.out.println("Player 3 card is the " + getCardValueAndShuffleCard.showCard(newCard3));

				if (value(userCardInHand) > 21  || value(userCardInHand2) > 21 || value(userCardInHand3) > 21)
				{    String playerOverTweentOne = "";
				if(value(userCardInHand) > 21){
					playerOverTweentOne = "Player 1";
				}
				else if (value(userCardInHand2) > 21){
					playerOverTweentOne = "Player 2";
				}
				else{
					playerOverTweentOne = "Player 3";
				}
				System.out.println();
				System.out.println(playerOverTweentOne + "  Over 21 lose");
				System.out.println("Dealer's other card was the " + getCardValueAndShuffleCard.showCard(getCardValueAndShuffleCard.getCard(dealerCardInHand, 1)));
				return false;
				}
			}

		} 

		System.out.println();
		System.out.println("User stands.");
		System.out.println("Dealer's cards are");
		System.out.println("    " + getCardValueAndShuffleCard.showCard(getCardValueAndShuffleCard.getCard(dealerCardInHand, 0)));
		System.out.println("    " + getCardValueAndShuffleCard.showCard(getCardValueAndShuffleCard.getCard(dealerCardInHand, 1)));
		while (value(dealerCardInHand) <= 16)
		{
			int newCard = dealCard();
			System.out.println("Dealer hits and gets the " + getCardValueAndShuffleCard.showCard(newCard));
			dealerCardInHand.addElement(newCard);
		}
		System.out.println("Dealer's total is " + value(dealerCardInHand));

		/* Now, the winner can be declared. */

		System.out.println();
		if (value(dealerCardInHand) > 21)
		{
			System.out.println("Dealer busted.  You win.");
			return true;
		} else
		{
			if (value(dealerCardInHand) == value(userCardInHand))
			{
				System.out.println("Dealer wins on a tie.  You lose.");
				return false;
			} else
			{
				if (value(dealerCardInHand) > value(userCardInHand))
				{
					System.out.println("Dealer wins, " + value(dealerCardInHand) + " points to " + 
							value(userCardInHand) + ".");
					return false;
				} else
				{
					System.out.println("You win, " + value(userCardInHand) + " points to " + 
							value(dealerCardInHand) + ".");
					return true;
				}
			}
		}

	}  

	public int dealCard()
	{
		if (currentPosition == 52)
		{
			shuffleCards();
		}
		currentPosition++;
		return deck[currentPosition - 1];
	}

	public int getCard(Vector cardsInHand, int position)
	{

		if (position >= 0 && position < cardsInHand.size())
		{
			return ((Integer)cardsInHand.elementAt(position)).intValue();
		} else
		{
			return 0;
		}
	}



	public void shuffleCards()
	{
		for (int i = 51; i > 0; i--)
		{
			int rand = (int) (Math.random() * (i + 1));
			int temp = deck[i];
			deck[i] = deck[rand];
			deck[rand] = temp;
		}
		currentPosition = 0;
	}

	public int value(Vector cardsInHand)
	{

		int val;      
		boolean ace;  
		int cards;  

		val = 0;
		ace = false;
		cards = cardsInHand.size();

		for (int i = 0; i < cards; i++)
		{
			int card;    
			int cardVal;  
			card = getCard(cardsInHand, i);

			if(card > 10){
				card = 10;
			}
			cardVal = card;  
			if (cardVal > 10)
			{
				cardVal = 10;   
			}
			if (cardVal == 1)
			{
				ace = true;     
			}
			val = val + cardVal;
		}

		if (ace == true && val + 10 <= 21)
		{
			val = val + 10;
		}
		return val;
	}
}
